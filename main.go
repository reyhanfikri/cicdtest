package main

import (
	"fmt"
	"math"

	"gitlab.com/reyhanfikri/cicdtest/configs"
	"gitlab.com/reyhanfikri/cicdtest/internal/bispro"
)

func main() {
	calc := bispro.Calculator{}
	fmt.Println(calc.Add(5, 7))

	dbconf := configs.DBConfig{}
	dbconf.Initialize()
	circleArea, err := calc.CalcCircleArea(dbconf, 100)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(int(math.Round(circleArea)))
}

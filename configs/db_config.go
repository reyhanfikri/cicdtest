package configs

type PostgreConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DBName   string
}

type RedisConfig struct {
	Addr     string
	Password string
}

type DBConfig struct {
	Postgre PostgreConfig
	Redis   RedisConfig
}

func (DBConfig *DBConfig) Initialize() {
	DBConfig.Postgre.Host = "localhost"
	DBConfig.Postgre.Port = 5432
	DBConfig.Postgre.User = "postgres"
	DBConfig.Postgre.Password = "otomedomoyo"
	DBConfig.Postgre.DBName = "cicdtest"

	DBConfig.Redis.Addr = "localhost:6379"
	DBConfig.Redis.Password = "otomedomoyo"
}

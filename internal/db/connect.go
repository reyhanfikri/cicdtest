package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"

	"gitlab.com/reyhanfikri/cicdtest/configs"
)

func GetPostgreConnection(dbconf configs.DBConfig) (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbconf.Postgre.Host, dbconf.Postgre.Port, dbconf.Postgre.User,
		dbconf.Postgre.Password, dbconf.Postgre.DBName)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func GetRedisConnection(dbconf configs.DBConfig) *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr:     dbconf.Redis.Addr,
		Password: dbconf.Redis.Password,
	})
	return rdb
}

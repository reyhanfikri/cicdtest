package models

import (
	"context"

	"gitlab.com/reyhanfikri/cicdtest/configs"
	"gitlab.com/reyhanfikri/cicdtest/internal/db"
)

type CalculatorSymbol struct {
	ID    int     `redis:"id"`
	Name  string  `redis:"name"`
	Value float64 `redis:"value"`
}

func (cs *CalculatorSymbol) GetValue(dbconf configs.DBConfig, name string) error {
	ctx := context.Background()

	// Get from redis first
	redisConn := db.GetRedisConnection(dbconf)
	defer redisConn.Close()

	keyList, err := redisConn.Keys(ctx, name).Result()
	if err != nil {
		return err

	} else if len(keyList) == 0 {
		// If from redis is not found, then get from postgre
		postgreConn, err := db.GetPostgreConnection(dbconf)
		if err != nil {
			return err
		}
		defer postgreConn.Close()

		row := postgreConn.QueryRowContext(
			ctx,
			"select id, name, value from calculator.symbol where name = $1",
			name)
		if row.Err() != nil {
			return row.Err()
		}

		err = row.Scan(&cs.ID, &cs.Name, &cs.Value)
		if err != nil {
			return err
		}

		// insert it into redis
		err = redisConn.HMSet(ctx, name, cs).Err()
		if err != nil {
			return err
		}

		return nil

	} else {
		// If from redis is found, then get from redis
		err := redisConn.HGetAll(ctx, name).Scan(cs)
		if err != nil {
			return err
		}

		return nil
	}
}

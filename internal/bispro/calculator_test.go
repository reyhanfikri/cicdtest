package bispro_test

import (
	"context"
	"math"
	"os"
	"testing"

	"gitlab.com/reyhanfikri/cicdtest/configs"
	"gitlab.com/reyhanfikri/cicdtest/internal/bispro"
	"gitlab.com/reyhanfikri/cicdtest/internal/db"
)

func InitDatabaseGitlabCI(dbconf configs.DBConfig) error {
	dbConn, err := db.GetPostgreConnection(dbconf)
	if err != nil {
		return err
	}
	defer dbConn.Close()

	sqlByte, err := os.ReadFile("../../sql/cicdtest.sql")
	if err != nil {
		return err
	}

	_, err = dbConn.Exec(string(sqlByte))
	if err != nil {
		return err
	}
	return nil
}

func TestAdd(t *testing.T) {
	c := bispro.Calculator{}
	if c.Add(2, 3) != 5 {
		t.Fatalf("2 + 3 is not 5! Check the code again")
	}
}

func TestCalcCircleArea(t *testing.T) {
	// initialize db conf
	dbconf := configs.DBConfig{}
	dbconf.Initialize()
	if os.Getenv("GITLAB_CI") == "true" {
		dbconf.Postgre.Host = "postgres"
		err := InitDatabaseGitlabCI(dbconf)
		if err != nil {
			t.Fatalf("Error when initialize database on GITLAB CI. ERR: %s", err.Error())
		}
		dbconf.Redis.Addr = "redis:6379"
	}

	// delete redis key 'pi' first
	redisConn := db.GetRedisConnection(dbconf)
	defer redisConn.Close()

	ctx := context.Background()
	err := redisConn.Del(ctx, "pi").Err()
	if err != nil {
		t.Fatalf("Error when deleting key 'pi' in redis. ERR: %s", err.Error())
	}

	// do test postgres
	c := bispro.Calculator{}
	circleArea, err := c.CalcCircleArea(dbconf, 100)
	if err != nil {
		t.Fatalf("[Postgre Test] Error when calculate circle area with r = 100. ERR: %s", err.Error())
	}
	if int(math.Round(circleArea)) != 31416 {
		t.Fatalf("[Postgre Test] Circle with r = 100 should be 31416, not %d !", int(math.Round(circleArea)))
	}

	// do test redis
	circleArea, err = c.CalcCircleArea(dbconf, 100)
	if err != nil {
		t.Fatalf("[Redis Test] Error when calculate circle area with r = 100. ERR: %s", err.Error())
	}
	if int(math.Round(circleArea)) != 31416 {
		t.Fatalf("[Redis Test] Circle with r = 100 should be 31416, not %d !", int(math.Round(circleArea)))
	}
}

package bispro

import (
	"gitlab.com/reyhanfikri/cicdtest/configs"
	"gitlab.com/reyhanfikri/cicdtest/internal/models"
)

type Calculator struct{}

func (c *Calculator) Add(a float64, b float64) float64 {
	return a + b
}

func (c *Calculator) CalcCircleArea(dbconf configs.DBConfig, r float64) (float64, error) {
	pi := models.CalculatorSymbol{}
	err := pi.GetValue(dbconf, "pi")
	return pi.Value * r * r, err
}

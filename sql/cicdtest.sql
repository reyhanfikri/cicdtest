CREATE SCHEMA calculator;

CREATE TABLE calculator.symbol (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	value numeric NOT NULL,
	CONSTRAINT symbol_name_key UNIQUE (name),
	CONSTRAINT symbol_pkey PRIMARY KEY (id)
);

INSERT INTO calculator.symbol(id, "name", value) VALUES (1, 'pi', 3.14159);
